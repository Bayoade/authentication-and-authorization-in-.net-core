﻿using Auth.Sql.Models;
using Microsoft.EntityFrameworkCore;

namespace Auth.Sql
{
    public class DemoDbContext : DbContext
    {
        public DemoDbContext(DbContextOptions<DemoDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.HasDefaultSchema("dbo");

            var userModelBuilder = modelBuilder.Entity<User>().ToTable("Users");

            userModelBuilder.HasKey(x => x.Id);
        }

        internal DbSet<User> UserProfiles { get; set; }
    }
}
