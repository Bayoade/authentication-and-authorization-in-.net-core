﻿using Auth.Core.Helper;
using Auth.Core.IRepositry;
using Auth.Core.Models;
using Auth.Sql.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Auth.Sql.Repository
{
    public class UserProfileRepository : IUserProfileRepository
    {
        private readonly DemoDbContext _dbContext;

        public UserProfileRepository(DemoDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task CreateUserAsync(UserProfile profile, string hashedPassword, string salt)
        {
            var user = Mapper.Map<User>(profile);
            user.CreatedDate = DateTime.UtcNow;
            user.Password = hashedPassword;
            user.Salt = salt;

            _dbContext.UserProfiles.Add(user);
            return _dbContext.SaveChangesAsync();
        }

        public Task DeleteUserAsync(Guid id)
        {
            var user = new User { Id = id };

            _dbContext.UserProfiles.Attach(user);
            _dbContext.UserProfiles.Remove(user);

            return _dbContext.SaveChangesAsync();
        }

        public async Task<IList<UserProfile>> GetAllUserAsync()
        {
            var users = await _dbContext.UserProfiles.ToListAsync();

            return Mapper.Map<List<UserProfile>>(users);
        }

        public async Task<UserProfile> GetUserAsync(Guid id)
        {
            var user = await _dbContext.UserProfiles.FirstOrDefaultAsync(x => x.Id == id);

            return Mapper.Map<UserProfile>(user);
        }

        public async Task<UserProfile> GetUserByEmailAsync(string email)
        {
            var user = await _dbContext.UserProfiles.FirstOrDefaultAsync(x => x.Email == email);

            return Mapper.Map<UserProfile>(user);
        }

        public async Task<UserDetails> GetUserDetailsAsync(string userName)
        {
            var user = await _dbContext.UserProfiles.FirstOrDefaultAsync(x => x.UserName == userName);

            if(user.IsNull())
            {
                return null;
            }

            return Mapper.Map<UserDetails>(user);
        }

        public async Task UpdateUserAsync(UserProfile profile)
        {
            var dbUser = _dbContext.UserProfiles.FindAsync(profile.Id);

            if (!dbUser.IsNull())
            {
                await Mapper.Map(profile, dbUser);
                dbUser.GetAwaiter().GetResult().Modified = DateTime.UtcNow;

                _dbContext.Update(dbUser);

                await _dbContext.SaveChangesAsync();
            }
        }
    }
}
