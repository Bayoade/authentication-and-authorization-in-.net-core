﻿using Auth.Core.Models;
using Auth.Sql.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Auth.Sql
{
    public class SqlMapperProfile : Profile
    {
        public SqlMapperProfile()
        {
            CreateMap<User, UserProfile>()
                .ReverseMap()
                .ForMember(x => x.CreatedDate, y => y.Ignore())
                .ForMember(x => x.Modified, y => y.Ignore())
                .ForMember(x => x.Password, y => y.Ignore())
                .ForMember(x => x.Salt, y => y.Ignore());

            CreateMap<User, UserDetails>();
        }
    }
}
