﻿using Auth.Sql;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace AuthenticationAuthorizationDemo.Bootstrap
{
    public static class ConfigureAutoMapper
    {
        public static void AddMapperProfile(this IServiceCollection services)
        {
            Mapper.Initialize(config =>
            {
                config.AddProfile<SqlMapperProfile>();
                config.AddProfile<WebMapperProfile>();
            });
        }
    }
}
