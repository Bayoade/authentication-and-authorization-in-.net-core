﻿using Auth.Sql;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace AuthenticationAuthorizationDemo.Bootstrap
{
    public static class ConfigureDbContext
    {
        public static void AddDbContextConfiguration(this IServiceCollection services, IConfiguration Config)
        {
            services.AddDbContext<DemoDbContext>(options =>
            {
                options.UseSqlServer(Config.GetConnectionString("DemoConnectionKey"));
            });
        }
    }
}
