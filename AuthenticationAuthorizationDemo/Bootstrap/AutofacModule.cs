﻿using Auth.Core.IRepositry;
using Auth.Core.IService;
using Auth.Service.Service;
using Auth.Sql.Repository;
using AuthenticationAuthorizationDemo.AppService;
using AuthenticationAuthorizationDemo.AppServices;
using Autofac;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthenticationAuthorizationDemo.Bootstrap
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UserProfileRepository>()
                .As<IUserProfileRepository>().InstancePerLifetimeScope();

            builder.RegisterType<UserProfileService>()
                .As<IUserProfileService>().InstancePerLifetimeScope();

            builder.RegisterType<AuthenticationUserService>()
                .As<IAuthenticationUserService>().InstancePerLifetimeScope();

            builder.RegisterType<CryptoServiceProvider>()
                .As<ICryptoServiceProvider>().InstancePerLifetimeScope();

            builder.RegisterType<HttpContextAccessor>()
                .As<IHttpContextAccessor>();

            builder.RegisterType<UserContext>()
                .As<IUserContext>()
                .InstancePerLifetimeScope();
        }
    }
}
