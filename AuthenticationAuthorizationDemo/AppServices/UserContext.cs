﻿using Auth.Core.IService;
using Auth.Core.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using System.Security.Claims;

namespace AuthenticationAuthorizationDemo.AppServices
{
    public class UserContext : IUserContext
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUserProfileService _userProfileService;

        public UserContext(
            IHttpContextAccessor httpContextAccessor,
            IUserProfileService userProfileService)
        {
            _httpContextAccessor = httpContextAccessor;
            _userProfileService = userProfileService;
        }

        public string UserName()
        {
            return _httpContextAccessor.HttpContext.User.Claims
                .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value;
        }

        public bool IsAuthenticated()
        {
            return _httpContextAccessor.HttpContext.User.Identity.IsAuthenticated;
        }

        public string Email()
        {
            return GetUserProfile().Email;
        }

        public Guid UserId()
        {
            return GetUserProfile().Id;
        }

        private UserProfile GetUserProfile()
        {
            return _userProfileService.GetUserByEmailAsync(UserName())
                .GetAwaiter().GetResult();
        }
    }
}
