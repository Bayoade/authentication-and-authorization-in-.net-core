﻿using Auth.Core.Helper;
using Auth.Core.IRepositry;
using Auth.Core.IService;
using Auth.Core.Models;
using System;
using System.Threading.Tasks;

namespace AuthenticationAuthorizationDemo.AppService
{
    public class AuthenticationUserService : IAuthenticationUserService
    {
        private readonly IUserProfileRepository _userProfileRepository;
        private readonly ICryptoServiceProvider _cryptoServiceProvider;
        private readonly IUserProfileService _userProfileService;

        public AuthenticationUserService(
            IUserProfileRepository userProfileRepository, 
            ICryptoServiceProvider cryptoServiceProvider,
            IUserProfileService userProfileService)
        {
            _userProfileRepository = userProfileRepository;
            _cryptoServiceProvider = cryptoServiceProvider;
            _userProfileService = userProfileService;
        }

        public async Task<ValidUser> AuthenticateUserAsync(UserSummary summary)
        {
            var userDetails = await _userProfileRepository.GetUserDetailsAsync(summary.UserName);

            if(userDetails.IsNull())
            {
                return null;
            }

            var hashedPassword = _cryptoServiceProvider.ConvertPasswordToSha256(summary.Password, userDetails.Salt, 10000);

            if (hashedPassword != userDetails.Password)
            {
                return null;
            }
            return new ValidUser { UserName = summary.UserName, Email = userDetails.Email };
        }

        public Task CreateUserProfileAsync(UserSecret secret, UserProfile profile)
        {
            var salt = _cryptoServiceProvider.GenerateSalt(16);
            var hashedPassword = _cryptoServiceProvider.ConvertPasswordToSha256(secret.Password, salt, 10000);

            return _userProfileService.CreateUserAsync(profile, hashedPassword, salt);
        }
    }
}
