﻿using Auth.Core.Models;
using AuthenticationAuthorizationDemo.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthenticationAuthorizationDemo
{
    public class WebMapperProfile : Profile
    {
        public WebMapperProfile()
        {
            CreateMap<ValidUser, RegisterModel>()
                .ForMember(x => x.Address, y => y.Ignore())
                .ForMember(x => x.FirstName, y => y.Ignore())
                .ForMember(x => x.LastName, y => y.Ignore())
                .ForMember(x => x.ConfirmPassword, y => y.Ignore())
                .ForMember(x => x.PhoneNumber, y => y.Ignore())
                .ForMember(x => x.Password, y => y.Ignore());

            CreateMap<UserSecret, RegisterModel>()
                .ForMember(x => x.Address, y => y.Ignore())
                .ForMember(x => x.FirstName, y => y.Ignore())
                .ForMember(x => x.LastName, y => y.Ignore())
                .ForMember(x => x.UserName, y => y.Ignore())
                .ForMember(x => x.PhoneNumber, y => y.Ignore())
                .ForMember(x => x.Email, y => y.Ignore());

            CreateMap<UserProfile, RegisterModel>()
                .ForMember(x => x.ConfirmPassword, y => y.Ignore())
                .ForMember(x => x.Password, y => y.Ignore())
                .ReverseMap();
        }
    }
}
