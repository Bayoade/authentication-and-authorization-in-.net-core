﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Auth.Core.Helper;
using Auth.Core.IRepositry;
using Auth.Core.IService;
using Auth.Core.Models;
using AuthenticationAuthorizationDemo.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AuthenticationAuthorizationDemo.Controllers
{
    public class AuthController : Controller
    {
        private readonly IUserProfileService _userProfileService;
        private readonly IAuthenticationUserService _authenticationUserService;
        public AuthController(
            IUserProfileService userProfileService,
            IAuthenticationUserService authenticationUserService)
        {
            _userProfileService = userProfileService;
            _authenticationUserService = authenticationUserService;
        }

        public IActionResult Login(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");

            ViewBag.ReturnUrl = returnUrl;

            return View();
        }

        public IActionResult AccessDenied()
        {
            return View();
        }

        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (!ModelState.IsValid && model.ConfirmPassword != model.Password)
            {
                ModelState.AddModelError("Error", "Invalid Model or Password Inconsistency");
                return View(model);
            }

            var userProfile = Mapper.Map<UserProfile>(model);

            await _authenticationUserService.CreateUserProfileAsync(Mapper.Map<UserSecret>(model), userProfile);

            await SignInAsync(Mapper.Map<ValidUser>(model));

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginModel model, string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            var userSummary = Mapper.Map<UserSummary>(model);

            var validUser = await _authenticationUserService.AuthenticateUserAsync(userSummary);

            if (validUser.IsNull())
            {
                return View(model);
            }

            await SignInAsync(validUser);

            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction("Login");
        }

        [Authorize]
        public async Task<IActionResult> UserProfiles()
        {
            var users = await _userProfileService.GetAllUserAsync();
            return View(users);
        }

        private async Task SignInAsync(ValidUser model)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, model.UserName),
            };

            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

            await HttpContext.SignInAsync(claimsPrincipal);
        }
    }
}