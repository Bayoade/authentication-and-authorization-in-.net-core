﻿using Auth.Core.IRepositry;
using Auth.Core.IService;
using Auth.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Auth.Service.Service
{
    public class UserProfileService : IUserProfileService
    {
        private readonly IUserProfileRepository _userProfileRepository;
        public UserProfileService(IUserProfileRepository userProfileRepository)
        {
            _userProfileRepository = userProfileRepository;
        }
        public Task CreateUserAsync(UserProfile profile, string hashedPassword, string salt)
        {
            profile.Id = Guid.NewGuid();
            return _userProfileRepository.CreateUserAsync(profile, hashedPassword, salt);
        }

        public Task DeleteUserAsync(Guid id)
        {
            return _userProfileRepository.DeleteUserAsync(id);
        }

        public Task<IList<UserProfile>> GetAllUserAsync()
        {
            return _userProfileRepository.GetAllUserAsync();
        }

        public Task<UserProfile> GetUserAsync(Guid id)
        {
            return _userProfileRepository.GetUserAsync(id);
        }

        public Task<UserProfile> GetUserByEmailAsync(string email)
        {
            return _userProfileRepository.GetUserByEmailAsync(email);
        }

        public Task UpdateUserAsync(UserProfile profile)
        {
            return _userProfileRepository.UpdateUserAsync(profile);
        }
    }
}
