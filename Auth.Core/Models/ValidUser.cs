﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Auth.Core.Models
{
    public class ValidUser
    {
        public string UserName { get; set; }

        public string Email { get; set; }
    }
}
