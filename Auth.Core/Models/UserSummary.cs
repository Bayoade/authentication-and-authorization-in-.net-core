﻿using System;

namespace Auth.Core.Models
{
    public class UserSummary
    {
        public string UserName { get; set; }

        public string Password { get; set; }
    }
}
