﻿namespace Auth.Core.IService
{
    public interface ICryptoServiceProvider
    {
        string GenerateSalt(int maxLength);

        string ConvertPasswordToSha256(string password, string salt, int iterations);
    }
}
