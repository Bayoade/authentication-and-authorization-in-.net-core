﻿using Auth.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Auth.Core.IService
{
    public interface IUserProfileService
    {
        Task CreateUserAsync(UserProfile profile, string hashedPassword, string salt);

        Task DeleteUserAsync(Guid id);

        Task UpdateUserAsync(UserProfile profile);

        Task<UserProfile> GetUserAsync(Guid id);

        Task<IList<UserProfile>> GetAllUserAsync();

        Task<UserProfile> GetUserByEmailAsync(string email);
    }

    
}
