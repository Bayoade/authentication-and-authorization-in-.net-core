﻿using System;

namespace Auth.Core.IService
{
    public interface IUserContext
    {
        Guid UserId();

        bool IsAuthenticated();

        string Email();

        string UserName();
    }
}
