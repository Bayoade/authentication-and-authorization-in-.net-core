﻿using Auth.Core.Models;
using System;
using System.Threading.Tasks;

namespace Auth.Core.IService
{
    public interface IAuthenticationUserService
    {
        Task<ValidUser> AuthenticateUserAsync(UserSummary userSummary);

        Task CreateUserProfileAsync(UserSecret secret, UserProfile profile);
    }
}
