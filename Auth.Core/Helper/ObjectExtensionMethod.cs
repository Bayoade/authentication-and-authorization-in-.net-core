﻿namespace Auth.Core.Helper
{
    public static class ObjectExtensionMethod
    {
        public static bool IsNull(this object source)
        {
            return source == null;
        } 
    }
}
