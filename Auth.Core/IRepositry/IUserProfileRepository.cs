﻿using Auth.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Auth.Core.IRepositry
{
    public interface IUserProfileRepository
    {
        Task CreateUserAsync(UserProfile profile, string hashedPassword, string salt);

        Task DeleteUserAsync(Guid id);

        Task UpdateUserAsync(UserProfile profile);

        Task<UserProfile> GetUserAsync(Guid id);

        Task<IList<UserProfile>> GetAllUserAsync();

        Task<UserDetails> GetUserDetailsAsync(string userName);

        Task<UserProfile> GetUserByEmailAsync(string email);
    }
}
